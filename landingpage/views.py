from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.contrib.auth import logout
from social_django.models import UserSocialAuth
from .forms import *
from .models import *
import requests

response = {}
mhs_name = 'Razaqa Dhafin Haffiyan'
univ = 'Universitas Indonesia'
hobby = 'Make Something'
desc = 'I am a big dreamer'
npm = 1706039484
img_url = 'https://bem.cs.ui.ac.id/staf/assets/2018/img/photo/1706039484.jpg'
bckgrd_url = 'https://images.adsttc.com/media/images/5017/aeef/28ba/0d44/3100/0c6d/slideshow/stringio.jpg?1414573411'

def index(request):
    response['data'] = Status.objects.all()
    response['form'] = Status_Form()
    response['max-length'] = 300
    html = 'index.html'
    if 'counter' not in request.session:
        request.session['counter'] = 0
    response['counter'] = request.session['counter']
    return render(request, html, response)

def out(request):
    logout(request)
    request.session.flush()
    return HttpResponseRedirect('/')

def create_status(request):
    form = Status_Form(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        response['post'] = request.POST['post']
        status = Status(post = response['post'])
        status.save()
    return HttpResponseRedirect('/')

def profile(request):
    response = {'name': mhs_name, 'univ':univ, 'npm': npm, 'desc':desc,
                'hobby':hobby, 'img_url':img_url, 'bckgrd_url':bckgrd_url}
    html = 'profile.html'
    return render(request, html, response)

def get_books_api(request, change):
    URL = 'https://www.googleapis.com/books/v1/volumes?q=' + change
    req = requests.get(url = URL)
    data = req.json()
    return JsonResponse(data)

def subscribe_form(request):
    response['data'] = Subscribers.objects.all()
    response['form'] = Subscribe_Form()
    html = 'subscribe.html'
    return render(request, html, response)

def subscribe(request):
    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
        password = request.POST['password']
        subscriber = Subscribers(name=name, email=email, password=password)
        subscriber.save()
        return JsonResponse(subscriber.as_dict())

def validate_email(request):
    email = request.GET['email']
    data = {
        'is_taken': Subscribers.objects.filter(email=email).exists()
    }
    return JsonResponse(data)

def subscribers_list(request):
    subs = list(Subscribers.objects.values())
    data = {
        'subs': subs
    }
    return JsonResponse(data)

def unsubscribe(request):
    if request.method == 'POST':
        email = request.POST['email']
        subscriber = Subscribers.objects.filter(email=email)
        if subscriber.exists():
            subscriber.delete()
            msg = "delete success"
        else:
            msg = "delete unsuccess"
        resp = {
            'msg': msg
        }
        return JsonResponse(resp)

def favorite(request):
    request.session['counter'] = request.session['counter'] + 1
    return HttpResponse(request.session['counter'], content_type = 'application/json')

def unfavorite(request):
    request.session['counter'] = request.session['counter'] - 1
    return HttpResponse(request.session['counter'], content_type = 'application/json')
